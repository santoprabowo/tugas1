// Terdapat suatu array of object seperti berikut.
// 80
// 85
// 89
// 80
var arrObj = [
    { nama: 'farhan', nilai: 80, mapel: 'matematika' },
    { nama: 'akbar', nilai: 85, mapel: 'IPA' },        
    { nama: 'hanif', nilai: 89, mapel: 'IPS' },        
    { nama: 'adin', nilai: 80, mapel: 'matematika' }
];

console.log(arrObj[0].nilai)
console.log(arrObj[1].nilai)
console.log(arrObj[2].nilai)
console.log(arrObj[3].nilai)